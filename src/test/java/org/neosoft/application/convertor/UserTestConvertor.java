package org.neosoft.application.convertor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.neosoft.application.dto.UserDTO;
import org.neosoft.application.entity.User;
import org.neosoft.application.repository.UserRepository;
import org.neosoft.application.util.DateTimeUtil;


@ExtendWith(MockitoExtension.class)
public class UserTestConvertor {
	
	@Mock
	UserRepository userRepository;
	
	@InjectMocks
	UserConvertor userConvertor;
	
	public void givenUserDTO_whenDtoToEntity_thenReturnUser()
	{
		//given
		UserDTO userDTO=UserDTO.builder().surName("Mr.").firstName("Ajit").lastName("Kumar").dateOfBirth("19/07/1999").joiningDate("10/10/2021").phoneNumber(1111111111).emailId("ajit@gmail.com").isDiscard(0).build();
		
		//when
		Mockito.when(userRepository.save(Mockito.any())).thenReturn(User.builder().firstName("Ajit").build());
		
		User user=userConvertor.dtoToEntity(userDTO);
		
		//then
		Assertions.assertNotNull(user);
		Assertions.assertEquals(userDTO.getFirstName(), user.getFirstName());
	}
	
	public void givenUser_whenEntityToDTO_thenReturnUserDTO()
	{
		//given
		User user=User.builder().surName("Mr.").firstName("Ajit").lastName("Kumar").dateOfBirth(DateTimeUtil.strToDate("19/06/1996")).dateOfBirth(DateTimeUtil.strToDate("10/10/2021")).phoneNumber(1111111111).emailId("ajit@gmail.com").isDiscard(0).build();
		
		//when
		Mockito.when(userRepository.save(user)).thenReturn(User.builder().dateOfBirth(DateTimeUtil.strToDate("19/06/1996")).build());
		
		UserDTO userDTO=userConvertor.entityToDTO(user);
		
		Assertions.assertNotNull(userDTO);
		Assertions.assertEquals(user.getFirstName(), userDTO.getFirstName());
		
	}
	
	

}
