package org.neosoft.application.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.neosoft.application.controller.UserController;
import org.neosoft.application.entity.User;
import org.neosoft.application.repository.UserRepository;
import org.neosoft.application.util.DateTimeUtil;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@WebMvcTest(value = UserService.class)
public class UserTestServiceImpl {
	
	@MockBean
	UserRepository userRepository;
	
	@MockBean
	private UserService userService;
	
	@Test
	public void getAllUserTest()
	{
		Mockito.when(userService.getAllUser()).thenReturn(Stream.
				of(new User(1, "Rana", "Ajit", "Kumar", DateTimeUtil.strToDate("02/10/1996"), DateTimeUtil.strToDate("20/10/2021"), "Khanpur", "Delhi", "South Delhi", "11111", 0, 1234567890, "ajit@gmail.com"),
						new User(2, "Rana", "Amit", "Kumar", DateTimeUtil.strToDate("02/12/1996"), DateTimeUtil.strToDate("18/10/2021"), "Khanpur", "Delhi", "South Delhi", "11111", 0, 1234567890, "ajit@gmail.com")).collect(Collectors.toList()));
		
		assertEquals(2, userService.getAllUser().size());
	}
	
	@Test
	public void gevenUserObject_thenSaveUser()
	{
		User user=new User(3, "Rana", "Ankit", "Kumar", DateTimeUtil.strToDate("02/12/1996"), DateTimeUtil.strToDate("18/10/2021"), "Khanpur", "Delhi", "South Delhi", "11111", 0, 1234567892, "ajit@gmail.com");
		
		Mockito.when(userService.save(user)).thenReturn(user);
		
		assertEquals(user, userService.save(user));
	}
	
	@Test
	public void givenUserId_thenGetUser()
	{
		
		User user=new User(5, "Rana", "Amman", "Kumar", DateTimeUtil.strToDate("02/12/1996"), DateTimeUtil.strToDate("18/10/2021"), "Khanpur", "Delhi", "South Delhi", "11111", 0,  1234567892, "ajit@gmail.com");
		
		Mockito.when(userService.getUserById(user.getId())).thenReturn(user);
		
		assertEquals(user, userService.getUserById(user.getId()));
	}
	
	public void givenId_whenUserGet_thenDeleteUser()
	{
		User user=new User(6, "Rana", "Amman", "Kumar", DateTimeUtil.strToDate("02/12/1996"), DateTimeUtil.strToDate("18/10/2021"), "Khanpur", "Delhi", "South Delhi", "11111", 0,  1234567892, "ajit@gmail.com");
		userService.deleteById(user.getId());
		verify(userRepository,times(1)).delete(user);
	}

}
