package org.neosoft.application.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.neosoft.application.convertor.UserConvertor;
import org.neosoft.application.entity.User;
import org.neosoft.application.repository.UserRepository;
import org.neosoft.application.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(value = UserController.class)
public class UserTestController {

	@MockBean
	UserRepository userRepository;

	@MockBean
	UserConvertor userConvertor;

	@MockBean
	private UserService userService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void given_whenGetRequestUserWithoutException_thenReturnListOfUser() throws Exception {

		Mockito.when(userRepository.findByIsDiscard(0)).thenReturn(new ArrayList<>());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/rest/user").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andReturn();

		System.out.println(result.getResponse());
		verify(userRepository, times(1)).findByIsDiscard(0);

	}

	@Test
	public void given_whenGetRequestUserWithException_thenReturnListOfUser() throws Exception {

		Mockito.when(userRepository.findByIsDiscard(0)).thenThrow(new RuntimeException());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/rest/user").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andReturn();
		MockHttpServletResponse response = result.getResponse();

		verify(userRepository, times(1)).findByIsDiscard(0);
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());

	}

	@Test
	public void gievnUserDTOJson_whenPostRequestUserWithoutException_thenReturnSuccessData() throws Exception {
		String json = "{\"id\": 1,\r\n" + "    \"surName\": \"Saw\",\r\n" + "    \"firstName\": \"Ajit\",\r\n"
				+ "    \"lastName\": \"Kumar\",\r\n" + "    \"dateOfBirth\": \"25/02/1994\",\r\n"
				+ "    \"joiningDate\": \"01/10/2021\",\r\n" + "    \"isDiscard\": 0,\r\n"
				+ "    \"phoneNumber\": 1111111111,\r\n" + "    \"emailId\": \"ajit@gmail.com\"\r\n" + "}";

		Mockito.when(userService.save(Mockito.any())).thenReturn(new User());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/rest/user").accept(MediaType.APPLICATION_JSON)
				.content(json).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.CREATED.value(), response.getStatus());

		verify(userService, times(1)).save(Mockito.any());

	}
	
	
	@Test
	public void gievnUserDTOJsonWithMissingSomeValidation_whenPostRequestUserWithoutException_thenReturnBadRequestData() throws Exception {
		String json = "{\"id\": 1,\r\n" + "    \"surName\": \"Saw\",\r\n" + "    \"firstName\": \"Ajit\",\r\n"
				+ "    \"lastName\": \"Kumar\",\r\n" + "    \"dateOfBirth\": \"25/02/1994\",\r\n"
				+ "    \"joiningDate\": \"01/10/2021\",\r\n" + "    \"isDiscard\": 0,\r\n"
				+ "    \"phoneNumber\": 1111111111,\r\n" +  "}";

		Mockito.when(userService.save(Mockito.any())).thenReturn(new User());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/rest/user").accept(MediaType.APPLICATION_JSON)
				.content(json).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());


	}
	
	@Test
	public void gievnUserDTOJson_whenPostRequestUserException_thenReturnException() throws Exception {
		String json = "{\"id\": 1,\r\n" + "    \"surName\": \"Saw\",\r\n" + "    \"firstName\": \"Ajit\",\r\n"
				+ "    \"lastName\": \"Kumar\",\r\n" + "    \"dateOfBirth\": \"25/02/1994\",\r\n"
				+ "    \"joiningDate\": \"01/10/2021\",\r\n" + "    \"isDiscard\": 0,\r\n"
				+ "    \"phoneNumber\": 1111111111,\r\n" + "    \"emailId\": \"ajit@gmail.com\"\r\n" + "}";

		Mockito.when(userService.save(Mockito.any())).thenThrow(new  RuntimeException());

		// Send course as body to /user
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/rest/user").accept(MediaType.APPLICATION_JSON)
				.content(json).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());

		

	}
	
	
	
	
	
	
	@Test
	public void gievnUserDTOJsonWithId_whenPutRequestUserException_thenReturnException() throws Exception {
		String json = "{\"id\": 1,\r\n" + "    \"surName\": \"Saw\",\r\n" + "    \"firstName\": \"Ajit\",\r\n"
				+ "    \"lastName\": \"Kumar\",\r\n" + "    \"dateOfBirth\": \"25/02/1994\",\r\n"
				+ "    \"joiningDate\": \"01/10/2021\",\r\n" + "    \"isDiscard\": 0,\r\n"
				+ "    \"phoneNumber\": 1111111111,\r\n" + "    \"emailId\": \"ajit@gmail.com\"\r\n" + "}";

		Mockito.when(userService.save(Mockito.any())).thenThrow(new  RuntimeException());

		// Send course as body to /user
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/rest/user/{id}",1).accept(MediaType.APPLICATION_JSON)
				.content(json).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());

		

	}
	
	@Test
	public void gievnUserDTOJsonWithId_whenPutRequestUserNull_thenReturnException() throws Exception {
		String json = "{\"id\": 1,\r\n" + "    \"surName\": \"Saw\",\r\n" + "    \"firstName\": \"Ajit\",\r\n"
				+ "    \"lastName\": \"Kumar\",\r\n" + "    \"dateOfBirth\": \"25/02/1994\",\r\n"
				+ "    \"joiningDate\": \"01/10/2021\",\r\n" + "    \"isDiscard\": 0,\r\n"
				+ "    \"phoneNumber\": 1111111111,\r\n" + "    \"emailId\": \"ajit@gmail.com\"\r\n" + "}";

		Mockito.when(userService.save(Mockito.any())).thenReturn(null);

		// Send course as body to /user
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/rest/user/{id}",1).accept(MediaType.APPLICATION_JSON)
				.content(json).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());

		

	}
	
	
	@Test
	public void givenId_whenPatchRequestUserWithoutException_thenReturnUser() throws Exception {

		Mockito.when(userService.getUserById(Mockito.anyInt())).thenReturn(new User());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.patch("/rest/user/{id}",1).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		//verify(userRepository, times(1)).findAll();

	}
	
	@Test
	public void givenId_whenPatchRequestUserException_thenReturnException() throws Exception {

		Mockito.when(userService.getUserById(Mockito.anyInt())).thenThrow(new RuntimeException());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.patch("/rest/user/{id}",1).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		

	}
	
	@Test
	public void givenId_whenPatchRequestUserNull_thenReturnUserNotFound() throws Exception {

		Mockito.when(userService.getUserById(Mockito.anyInt())).thenReturn(null);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.patch("/rest/user/{id}",1).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
		

	}
	
	
	@Test
	public void givenId_whenDeleteRequestUserWithOutException_thenReturnDeleteMassage() throws Exception {

		Mockito.when(userService.getUserById(Mockito.anyInt())).thenReturn(new User());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/rest/user/{id}",1).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		

	}
	
	
	@Test
	public void givenId_whenDeleteRequestUserWithException_thenReturnException() throws Exception {

		Mockito.when(userService.getUserById(Mockito.anyInt())).thenThrow(new RuntimeException());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/rest/user/{id}",1).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		

	}
	
	@Test
	public void givenId_whenDeleteRequestUserWithNull_thenReturnUserNotFound() throws Exception {

		Mockito.when(userService.getUserById(Mockito.anyInt())).thenReturn(null);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/rest/user/{id}",1).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
		

	}
	
	
	@Test
	public void givenId_whenSoftDeleteRequestUserWithOutException_thenReturnSoftDeleteMassage() throws Exception {

		Mockito.when(userService.getUserById(Mockito.anyInt())).thenReturn(new User());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/rest/soft_delete/{id}",1).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		verify(userService, times(1)).getUserById(1);

	}
	
	
	@Test
	public void givenId_whenSoftDeleteRequestUserWithException_thenReturnException() throws Exception {

		Mockito.when(userService.getUserById(Mockito.anyInt())).thenThrow(new RuntimeException());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/rest/soft_delete/{id}",1).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		verify(userService, times(1)).getUserById(1);

	}
	
	@Test
	public void givenId_whenSoftDeleteRequestUserWithNull_thenReturnUserNotFound() throws Exception {

		Mockito.when(userService.getUserById(Mockito.anyInt())).thenReturn(null);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/rest/soft_delete/{id}",1).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andDo(print()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
		verify(userService, times(1)).getUserById(1);

	}
	
	
	

}
