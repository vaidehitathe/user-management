package org.neosoft.application.dto;



import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
	
	private Integer id;
	@NotEmpty(message = "SurName not be empty")
	private String surName;
	@NotEmpty(message = "First Name not be empty")
	private String firstName;
	
	private String lastName;
	
	@NotEmpty(message = "Date Of Birth not be empty")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String dateOfBirth;
	@NotEmpty(message = "Date of joining not be empty")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String joiningDate;
	
	private String address;
	
	private String state;
	
	private String city;
	
	private String pincode;
	
	private Integer isDiscard;
	
	
	
	@NotNull(message = "Phone Number not be empty")
	private Integer phoneNumber;
	
	@NotEmpty(message = "Email Id Number not be empty")
	private String emailId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Integer getIsDiscard() {
		return isDiscard;
	}

	public void setIsDiscard(Integer isDiscard) {
		this.isDiscard = isDiscard;
	}

	public Integer getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	


}

