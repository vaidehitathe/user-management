package org.neosoft.application.repository;

import java.util.List;

import org.neosoft.application.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


//https://www.baeldung.com/spring-data-jpa-pagination-sorting
//https://devwithus.com/crud-api-with-spring-boot-jpa-hibernate-mysql/

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	
	 List<User> findByIsDiscard(Integer isDiscard);
	
	 
	 @Query(value = "Select * from app_user u WHERE u.sur_name like  CONCAT('%', ?1, '%') ",nativeQuery=true)
	 Page<User> findBySurNameLike(String surName ,Pageable pageable);
	 
	 @Query(value = "Select * from app_user u WHERE u.pincode like  CONCAT('%', ?1, '%') ",nativeQuery=true)
	 Page<User> findByPincode(String pincode ,Pageable pageable);
	 
	 
	 @Query(value = "Select * from app_user u WHERE u.first_name like  CONCAT('%', ?1, '%') ",nativeQuery=true)
	 Page<User> findByName(String name,Pageable pageable);
	 
	 
	 @Query(value = "Select * from app_user u WHERE u.first_name like  CONCAT('%', ?1, '%') and u.sur_name like  CONCAT('%', ?2, '%') and u.pincode like  CONCAT('%', ?3, '%')", nativeQuery=true)
	 List<User> findByNameAndLastNameAndPincode(String name,String surName,String pincode);
	
	
}
