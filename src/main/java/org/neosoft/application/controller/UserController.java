package org.neosoft.application.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.neosoft.application.convertor.UserConvertor;
import org.neosoft.application.dto.UserDTO;
import org.neosoft.application.entity.User;
import org.neosoft.application.repository.UserRepository;
import org.neosoft.application.service.UserService;
import org.neosoft.application.util.DateTimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import lombok.RequiredArgsConstructor;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	UserConvertor userConvertor;

	@Autowired
	UserService userService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	Gson gson;

	// Getting all Record

	@GetMapping(value = "/user")
	public ResponseEntity<List> getAllUser() {
		logger.info("--All user list--");
		List<User> userList = null;
		try {
			userList = userRepository.findByIsDiscard(0);
			List<UserDTO>	response=userList.stream().map(this::convertToResponse).collect(Collectors.toList());
			return new ResponseEntity<>(response, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(java.util.Arrays.asList("Some thing wrong"), HttpStatus.BAD_REQUEST);
		}

	}
	
	public UserDTO convertToResponse(User user)
	{
		UserDTO dto=new UserDTO();
		dto.setId(user.getId());
		dto.setFirstName(user.getFirstName());
		dto.setLastName(user.getLastName());
		dto.setSurName(user.getSurName());
		dto.setPhoneNumber(user.getPhoneNumber());
		dto.setEmailId(user.getEmailId());
		dto.setAddress(user.getAddress());
		dto.setCity(user.getCity());
		dto.setState(user.getState());
		dto.setPincode(user.getPincode());
		if(user.getDateOfBirth() != null)
		{
		dto.setDateOfBirth(DateTimeUtil.dateToString(user.getDateOfBirth()));
		}else {
			dto.setDateOfBirth("N/A");
		}
		if(user.getJoiningDate() != null)
		{
		dto.setJoiningDate(DateTimeUtil.dateToString(user.getJoiningDate()));
		}else {
			dto.setJoiningDate("N/A");
		}
		return dto;
		
	}

	
	// Post Mapping

	@PostMapping(value = "/user")
	public ResponseEntity<String> addUser(@Valid @RequestBody UserDTO userDTO, HttpServletRequest request,
			HttpServletResponse response) {

		User user = null;
		logger.info("-- post mapping call--");
		try {
			user = userConvertor.dtoToEntity(userDTO);
			user = userService.save(user);
			response.setStatus(HttpStatus.CREATED.value());
			logger.info("-- post mapping call successfully--");
			return new ResponseEntity<>(gson.toJson(user), HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("-- Something went wrong --");
			return new ResponseEntity<>(gson.toJson("Something went wrong"), HttpStatus.BAD_REQUEST);

		}

		

	}

	// patch mapping getting the the data by id

	@PatchMapping(value = "/user/{id}")
	public ResponseEntity<String> getUserById(@PathVariable Integer id, HttpServletRequest request,
			HttpServletResponse response) {
		UserDTO userDTO = null;
		logger.info("-- PATCH mapping call--");
		try {

			User user = userService.getUserById(id);
			if (user != null) {
				userDTO = userConvertor.entityToDTO(user);
				response.setStatus(HttpStatus.OK.value());
				logger.info("-- PATCH mapping call successfully--");
				return new ResponseEntity<>(gson.toJson(userDTO), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(gson.toJson("User Not Available"), HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Map<String, String> map = new HashMap<String, String>();
			map.put("status", "Bad Request");
			map.put("message", "Record not found for this id");
			return new ResponseEntity<>(gson.toJson(map), HttpStatus.BAD_REQUEST);
		}
		

	}

	// update the data by using id
	@PutMapping(value = "/user/{id}")
	public ResponseEntity<String> updateUser(@RequestBody UserDTO userDTO, @PathVariable Integer id,
			HttpServletRequest request, HttpServletResponse response) {
		try {

			User user = userService.getUserById(id);
			if (user != null) {
				user = userConvertor.dtoToUpdateEntity(user, userDTO);
				userRepository.save(user);
				response.setStatus(HttpStatus.OK.value());
				return new ResponseEntity<>(gson.toJson(user), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(gson.toJson("User Not Available"), HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(gson.toJson("Something went wrong"), HttpStatus.BAD_REQUEST);
		}
		

	}

	// hard delete the data by using id
	@DeleteMapping(value = "/user/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable("id") Integer id) {
		try {

			User user = userService.getUserById(id);
			if (user != null) {
				userService.deleteById(user.getId());
				logger.info("--hard delete call successfully--" + id);
				return new ResponseEntity<>(gson.toJson("User delete successfully"), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(gson.toJson("User Not Available"), HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(gson.toJson("Some thing is wrong"), HttpStatus.BAD_REQUEST);
		}

	}

	// Soft delete by id
	@PutMapping(value = "/soft_delete/{id}")
	public ResponseEntity<String> softDeleteUserById(@PathVariable("id") Integer id) {
		logger.info("--soft_delete call --" + id);
		try{
			User user = userService.getUserById(id);
			if (user != null) {
				user.setIsDiscard(1);
				userService.save(user);
				logger.info("--soft_delete call successfully--" + id);
				return new ResponseEntity<String>(gson.toJson("User delete successfully"), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(gson.toJson("User Not Available"), HttpStatus.NOT_FOUND);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(gson.toJson("Some thing is wrong"), HttpStatus.BAD_REQUEST);
		}
		
		
	}

	
	
	
	@GetMapping(value = "/search_and_sort_by_type")
	public ResponseEntity<String> serachByName(
			@RequestParam Optional<String> searchValue,
			@RequestParam Optional<Integer> page, 
			@RequestParam String sortByDateOfBirth,
			@RequestParam String joiningDate,
			@RequestParam String searchType, 
			HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("--search_by_name --" +searchValue +" and sort by "+ sortByDateOfBirth );
		Page<User> userList = null;
		try {
			Sort sort = Sort.by(
				    Sort.Order.asc(sortByDateOfBirth),
				    Sort.Order.asc(joiningDate));
			if(searchType.equalsIgnoreCase("firstName"))
			{
			userList=userRepository.findByName(searchValue.orElse(""), PageRequest.of(page.orElse(0), 10, sort));
			}else if(searchType.equalsIgnoreCase("Surname")){
				userList=userRepository.findBySurNameLike(searchValue.orElse(""), PageRequest.of(page.orElse(0), 10, sort));
			}else if(searchType.equalsIgnoreCase("pincode")) {
				userList=userRepository.findByPincode(searchValue.orElse(""), PageRequest.of(page.orElse(0), 10, sort));
			}
			return new ResponseEntity<>(gson.toJson(userList), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(gson.toJson("Some thing is wrong"), HttpStatus.BAD_REQUEST);
		}
		

	}
	
	
	
	@GetMapping(value = "/search_and_sort")
	public ResponseEntity<List> serachAndSort(
			@RequestParam String firstName,
			@RequestParam String lastName,
			@RequestParam String pincode,
			HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("--search_by_name --" +firstName +"" );
		List<User> userList = null;
		try {

		userList=userRepository.findByNameAndLastNameAndPincode(firstName,lastName,pincode);
			logger.info("--search_by_name --" +firstName +"" );
			List<UserDTO>	searchResponse=userList.stream().map(this::convertToResponse).collect(Collectors.toList());
			return new ResponseEntity<>(searchResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(java.util.Arrays.asList("Some thing is wrong"), HttpStatus.BAD_REQUEST);
			
		}
		

	}

}
